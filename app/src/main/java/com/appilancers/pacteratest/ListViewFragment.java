package com.appilancers.pacteratest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Project: pacteratest
 * Created by Aamir on 29/11/2016.
 * All rights reserved Appilancers
 */

public class ListViewFragment extends Fragment{

    // List View
    private ListView mList;

    // Adapter for List View
    private ListViewAdapter mListAdapter;

    // Progress Bar while data is being fetched
    private ProgressBar mProgressBar;

    // Text to display when there is not data to display
    private TextView mNoContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // BEGIN_INCLUDE (inflate_view)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_layout, container, false);

        mList = (ListView)view.findViewById(android.R.id.list);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mNoContent = (TextView) view.findViewById(R.id.no_content);

        mListAdapter = new ListViewAdapter(getActivity());
        mList.setAdapter(mListAdapter);

        return view;
    }
    // END_INCLUDE (inflate_view)

    // BEGIN_INCLUDE (setup_views)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadContent();

        // END_INCLUDE (setup_refreshlistener)
    }
    // END_INCLUDE (setup_views)

    private void loadContent() {

        Content.ListInfo dataList = new Content.ListInfo();

        List<Content.ItemInfo> dataItemList = new ArrayList<Content.ItemInfo>();
        dataItemList.add(new Content.ItemInfo("Car", "This is car", null));
        dataItemList.add(new Content.ItemInfo("bike", "This is bike", null));
        dataItemList.add(new Content.ItemInfo("plane", "This is plane", null));

        dataList.setItems(dataItemList);

        mListAdapter.setList(dataList);

    }

}
